/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.inheritanceshape;

/**
 *
 * @author nonnanee
 */
public class TestShap {
    public static void main(String[] args) {
        Circle circle1 = new Circle(3);
        circle1.print();
        
        Circle circle2 = new Circle(4);
        circle2.print();
        
        Rectangle rectangle1 = new Rectangle(3,4);
        rectangle1.print();
        
        Square square1 = new Square(2);
        square1.print();
        
        Triangle triangle1 = new Triangle(4,3);
        triangle1.print();

    }
}
