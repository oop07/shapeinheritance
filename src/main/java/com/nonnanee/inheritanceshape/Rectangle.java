/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.inheritanceshape;

/**
 *
 * @author nonnanee
 */
public class Rectangle {
    protected double height;
    protected double width;
    protected double side = 2 ;
  
    
    public Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
        this.side = side;
      
    }
    public double calArea(){
        return height * width;
    }
     public double calArea1(){
        return side * side;
     }
    public double getM(){
        return height;  
    }
    public double getSide(){
        return side;
    }
    public void print(){
        System.out.println("Rectangle: "+"height: "+this.height+','+"width: "
                +this.width+','+"area = "+height * width);
     }
}
