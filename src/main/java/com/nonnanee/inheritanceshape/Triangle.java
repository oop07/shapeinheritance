/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.inheritanceshape;

/**
 *
 * @author nonnanee
 */
public class Triangle {
   private double base;
   private double height;

    public static final double n = 0.5;

    public Triangle(double base, double height) {
        this.base = base;
        this.height = height;
    }
    
    public double calArea(){
        return 0.5 * base *height;
    }
    public double getT(){
        return base;
    }
    public void print(){
        System.out.println("Triangle: "+"base: "+this.base+','+"height: "+this.height+','
                +"area = "+0.5 * base *height);
     }
    
}
