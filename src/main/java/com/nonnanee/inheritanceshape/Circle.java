/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.nonnanee.inheritanceshape;

/**
 *
 * @author nonnanee
 */
public class Circle {
    private double r;
    public static final double pi = 22.0/7;
    public Circle(double r){
        this.r = r;
    }
    public double calArea(){
        return pi *r * r;
    }
    public double getR(){
        return r;
    }
    public void print(){
        System.out.println("Circle: "+"r: "+this.r+','+"r: "+this.r+','
                +"area = "+pi *r * r);
     }
    
}
